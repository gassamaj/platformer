﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

	public GameObject currentCheckpoint;

	private PlayerController player;

	public GameObject DeathParticle;
	public GameObject RespawnParticle;

	public int pointPenaltyOnDeath;

	public float  respawnDelay;

	private float gravityStore;

	//Use this for initialization
	void Start () {
		player = FindObjectOfType<PlayerController>();
	}

	//Update is called once per frame
	void Update () {
	}

	public void RespawnPlayer()
	{
		StartCoroutine ("RespawnPlayerCo");
	}

	public IEnumerator RespawnPlayerCo()
	{
		        Instantiate (DeathParticle, player.transform.position, player.transform.rotation);
				player.enabled = false;
				player.GetComponent<Renderer> ().enabled = false;
		        gravityStore = player.GetComponent<Rigidbody2D> ().gravityScale 
		        player.GetComponent<Rigidbody2D>.gravityScale = 0f;
		        player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
		        ScoreManager.AddPoints (-pointPenaltyOnDeath);
				Debug.Log ("Player Respawn");
		        yield return new WaitForSeconds(respawnDelay);
		        player.GetComponent<Rigidbody2D>.gravityScale = gravityStore; 
			    player.transform.position = currentCheckpoint.transform.position;
				player.enabled = true;
		        player.renderer.enabled = true;
				Instantiate (RespawnParticle, currentCheckpoint.transform.position, currentCheckpoint.transform.rotation);
		}

}